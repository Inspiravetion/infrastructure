/*
  TODO: seperate project config from 
 */

export let config = {
	//dist
	distDir: 'bin/dumby',

	//ts 
	tsconfig: '/src/tsconfig.json',
	
	//front src
    frontSrc: [
		'/src/*.ts',
        '/src/**/*.tsx',
        '/src/!(typings)/*.ts',
        '/src/typings/browser/**/*.d.ts',
        '/src/typings/browser.d.ts',
        '../jspm_packages/npm/inversify*/**/*.d.ts',
		'../jspm_packages/npm/mobx*/**/*.d.ts'
    ],
    //back src
    backSrc: [
		'/src/*.ts',
        '/src/!(typings)/*.ts',
        '/src/typings/main/**/*.d.ts',
        '/src/typings/main.d.ts'
    ],
    sassFiles: [
        '/src/**/*.scss'
    ],
	//projects 
	projects: [
		{ name: '..', isFront: false, restart: false },
	]
};