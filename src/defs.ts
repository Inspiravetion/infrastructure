export interface IMapping {
	from: string;
	to: string;
	toString(delim? : string);
}

export interface IShellClient {
	$(cmd: string, args?: string[], saveOutput?: boolean);
	$ave(cmd: string, args?: string[]);
	startRemoteSession(remoteUser: string, remoteAddress: string);
	endRemoteSession();
	remote(remoteUser: string, remoteAddress: string, session: (shell: IShellClient) => void);
	cd(path: string);
	mkdir(path: string);
	optFlag(flag: string, value: string): string;
	opt(value: string): string;
	beQuiet();
	beLoud();
}

export interface IDockerClient {
	pull(imageName: string);
	buildImage(imageName: string, workspace: string, dockerFile?: string, buildArgs?: IMapping[]);
	startContainer(containerName: string, imageName: string, ports: IMapping[], services?: IMapping[], volume?: IMapping, cmd?: string);
	attatchToContainer(containerName: string, cmd?: string);
	removeAllStoppedContainers();
	removeContainer(container_id: string);
	stopContainer(container_id: string);
	stopAllContainers();
	getAllUntaggedImages(): Promise<string[]>;
	getRunningContainers(): Promise<string[]>;
	getAllStoppedContainers(): Promise<string[]>;
	getAllContainers(): Promise<string[]>;
	getAllExitedContainers(): Promise<string[]>;
	removeImage(image_id: string);
	saveImage(image_id: string, out_file: string);
	removeAllUntaggedImages();
	resetDockerEnv();
	copyFromContainer(container: string, from: string, to: string);
	createContainer(container: string, image: string);
}

export interface IInfraClient {
	startDB();
	startWebApp(appName: string);
	startEcosystem();
	stopDB();
	stopWebApp();
	stopEcosystem();
	buildDB();
	buildWebApp(appName: string);
	buildWebAppDeploymentImage(appName: string);
	buildEcosystem();
	resetEnv();
	sshIntoClusterVM()
	scpBuildArtifactsToCluster();
	deployWebApp(appNAme: string);
}