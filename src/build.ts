import { config as defaultConfig } from './config';
import { exec, spawn } from 'child_process';
import { readFileSync } from 'fs';
import { join } from 'path'; 

const webpack = require('webpack');
const ts = require('gulp-typescript');
const typescript = require('typescript'); 
const sass = require('gulp-sass');

// get gulp instance from caller
let gulp;
let config = defaultConfig;

interface IProject {
	name: string;
	isFront: boolean;
	restart: boolean;
}

export function register(gulpInstance, configInstance) {
	gulp = gulpInstance;
	config = configInstance || config;

	function compile(proj: IProject) {
		let name    = proj.name,
			isFront = proj.isFront,
			restart = proj.restart;

		let compileTaskName = toCompileTaskName(name),
			compileTasks    = [],
			tsTaskName 		= toTSTaskName(name),
			restartTaskName = toRestartTaskName(name),
			sassTaskName 	= toSassTaskName(name);

		let typingsDistDir = `${config.distDir}/${name}/typings`,
			srcDistDir = `${config.distDir}/${name}/src`;

		if (isFront) {
			let compileFront = getFrontCompiler(name, srcDistDir, typingsDistDir);
			
			gulp.task(tsTaskName, () => {
				// setting up webpack front end compile task
				return compileFront();
			});
		} else {
			// setting up the commonjs ts compile task
			gulp.task(tsTaskName, () => {
				return compileTS(name, srcDistDir, typingsDistDir);
			});

			if (restart) { 
				// create the restart task and link it to the ts task
				gulp.task(restartTaskName, [tsTaskName], restartServer);			
			}
		}

		compileTasks.push(tsTaskName);


		// setup the sass compile task
		// if (isFront) {
		// 	gulp.task(sassTaskName, () => {
		// 		return compileSass(name, srcDistDir);
		// 	});
		// 	compileTasks.push(sassTaskName);
		// }

		// full compile task
		gulp.task(compileTaskName, compileTasks);

		return compileTaskName;
	}

	function tsFiles(name: string, isFront: boolean): string[] {
		let tsFiles;

		if (isFront) {
			tsFiles = config.frontSrc;
		} else {
			tsFiles = config.backSrc;
		}

		let files = tsFiles.map(path => {
			let prefix = './';

	        if (path[0] === '!') {
	            prefix = './' + path[0];
	            path = path.slice(1);
	        } else if (path.slice(0, 3) === '../') {
	            return prefix + path.slice(3);
	        }
			
			return prefix + name + path;
		});

		return files;
	}

	function sassFiles(name: string): string[] {
		return config.sassFiles.map(path => name + path);
	}

	function toCompileTaskName(name: string): string {
		return `compile::${name}`;
	}

	function toTSTaskName(name: string): string {
		return `compile::ts::${name}`;
	}

	function toSassTaskName(name: string): string {
		return `compile::sass::${name}`;
	}

	function toRestartTaskName(name: string): string {
		return `restart::server::after::${name}`;
	}

	function compileSass(name: string, srcDistDir: string) {
		return gulp.src(sassFiles(name))
			.pipe(sass().on('error', sass.logError))
			.pipe(gulp.dest(srcDistDir));
	}

	function compileTS(name: string, srcDistDir: string, typingsDistDir: string) {

		let tsConfig = JSON.parse(readFileSync(join(process.cwd(), name, config.tsconfig), 'utf8')).compilerOptions;
		tsConfig.typescript = typescript;

		let projFiles = tsFiles(name, false);

		let src = gulp.src(projFiles);

		let tsResult = src
			.pipe(ts(tsConfig));

		tsResult.dts.pipe(gulp.dest(typingsDistDir));

		return tsResult.js.pipe(gulp.dest(srcDistDir));
	}

	function getFrontCompiler(name: string, srcDistDir: string, typingsDistDir: string) {
		let compiler = webpack(require('./webpack').config(name));

		return () => {
			// so compilers stay in mem for caching
			return compiler.run(function(err, stats) {
				if(err) throw new Error(err);
				
				console.log(stats.toString({
					colors: true,
					chunkModules: false
				}));
			});
		};
	}

	let runningServer = null,
	    restarting = false;

	function restartServer() {
	    if (runningServer) {
	        console.log('Restarting service...');
	        restarting = true;
	        runningServer.kill();
	    } else {
	        console.log('Starting service...');
	    };

	    //shouldn't this be exec?
	    runningServer = spawn('npm', [ 'run', 'start']);

	    runningServer.stdout.on('data', (output) => console.log(output.toString()));
	    runningServer.stderr.on('data', (output) => console.log(output.toString()));

	    runningServer.on('exit', () => {
	        if (!restarting) {
	            console.log('Server has crashed!');
	            process.exit(-1);
	        }

	        restarting = false;
	    });
	}

	function installTypings() {
		config.projects.forEach((proj) => {
			exec('../../node_modules/.bin/typings install', { cwd: proj.name + '/src' }, (error, stdout, stderr) => {
			  if (error) {
			    console.error(`exec error: ${error}`);
			    return;
			  }
			  console.log(stdout);
			});
		});
	}

	gulp.task('typings', installTypings)

	let fullBuild = config.projects.map(proj => compile(proj));
	gulp.task('build', fullBuild);

	let fullTSBuild = config.projects.map(proj => toTSTaskName(proj.name));
	gulp.task('ts', fullTSBuild);

	let fullSassBuild = config.projects.filter(proj => proj.isFront).map(proj => toSassTaskName(proj.name));
	gulp.task('sass', fullSassBuild);

	gulp.task('start', restartServer);

	gulp.task('develop-build', fullBuild, () => {
		config.projects.forEach((proj) => {
			let projTSFiles = tsFiles(proj.name, proj.isFront),
				projTSTasks = [toTSTaskName(proj.name)];

			gulp.watch(projTSFiles, projTSTasks);
		});
	});	

	gulp.task('develop', fullBuild, () => {
		config.projects.forEach((proj) => {
			let projTSFiles   = tsFiles(proj.name, proj.isFront),
				projTSTasks   = [toTSTaskName(proj.name)]/*,
				projSassFiles = sassFiles(proj.name),
				projSassTasks = [toSassTaskName(proj.name)]*/;

			if (proj.restart) {
				projTSTasks.push(toRestartTaskName(proj.name));
			}

			// if (proj.isFront) {
			// 	gulp.watch(projSassFiles, projSassTasks);
			// }

			gulp.watch(projTSFiles, projTSTasks);
		});

		restartServer();
	});	

	gulp.task('default', ['develop']);
}