import * as program from 'commander';
import { join } from 'path';
import { InfrastructureClient } from './infrastructure_client';

let infrastructure = new InfrastructureClient();
let packageConfig = require(join(process.cwd(), './package.json'));

program
  .version('0.0.0')
  .option('-v, --verbose', 'verbose logs');
 
program
  .command('build [ db | me | prod | eco ]')
  .description('run build')
  .action(async function(target, options){
    switch (target) {
    	case "db":
    		await infrastructure.buildDB();
    		break;
    	case "me":
    		await infrastructure.buildWebApp(packageConfig.name)
    		break;
    	case "prod":
			  await infrastructure.buildWebAppDeploymentImage(packageConfig.name);
    		break;
      case "ci":
        await infrastructure.buildCI(packageConfig.name);
        break;
    	case "eco":
			  await infrastructure.buildEcosystem();
    		break;
    	default:
    		program.help();
    		break;
    }
  });

program
  .command('start [ db | me | eco ]')
  .description('start deployment container')
  .action(async function(target, options){
    switch (target) {
    	case "db":
    		await infrastructure.startDB();
    		break;
    	case "me":
        await infrastructure.buildWebApp(packageConfig.name);
        await infrastructure.buildWebAppDeploymentImage(packageConfig.name);
    		await infrastructure.startWebApp(packageConfig.name)
    		break;
    	case "eco":
			  await infrastructure.startEcosystem();
    		break;
    	default:
    		program.help();
    		break;
    }
  });

program
  .command('stop [ db | me | eco ]')
  .description('stop deployment container')
  .action(async function(target, options){
    switch (target) {
    	case "db":
    		await infrastructure.stopDB();
    		break;
    	case "me":
    		await infrastructure.stopWebApp(packageConfig.name);
    		break;
    	case "eco":
			  await infrastructure.stopEcosystem();
    		break;
    	default:
    		program.help();
    		break;
    }
  });

program
  .command('restart [ me ]')
  .description('stop deployment container')
  .action(async function(target, options){
    switch (target) {
      // case "db":
        // await infrastructure.stopDB();
        // break;
      case "me":
        await infrastructure.stopWebApp(packageConfig.name);
        await infrastructure.startWebApp(packageConfig.name);
        break;
      // case "eco":
      //   await infrastructure.stopEcosystem();
      //   break;
      default:
        program.help();
        break;
    }
  });

program
  .command('deploy [ me | ci ]')
  .description('stop deployment container')
  .action(async function(target, options){
    switch (target) {
      // case "db":
      //   await infrastructure.stopDB();
      //   break;
      case "me":
        await infrastructure.deployWebApp(packageConfig.name);
        break;
      case "ci":
        await infrastructure.deployCI(packageConfig.name);
        break;
      case "test":
        await infrastructure.testSSH();
        break;
      // case "eco":
      //   await infrastructure.stopEcosystem();
      //   break;
      default:
        program.help();
        break;
    }
  });

program
  .command('reset')
  .description('reset docker env')
  .action(async function(options){
    await infrastructure.resetEnv();
  });

program.parse(process.argv);