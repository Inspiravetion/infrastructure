const path = require('path');
const gulp = require('gulp');
const ts = require('gulp-typescript');
const typescript = require('typescript'); 
const fs = require('fs');

gulp.task('default', () => {
	let tsConfig = JSON.parse(fs.readFileSync(path.join(process.cwd(), 'tsconfig.json'), 'utf8')).compilerOptions;
	tsConfig.typescript = typescript;

	let src = gulp.src('./*.ts');

	let tsResult = src
		.pipe(ts(tsConfig));

	return tsResult.js.pipe(gulp.dest(tsConfig.outDir));
});