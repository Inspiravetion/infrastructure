import { join } from 'path';
import { optimize } from 'webpack';
import * as webpack from 'webpack';

export function config(project, prod=true) {
  return {

    entry: `./${project}/src/main.ts`,

    devtool: `source-map`,

    output: {
      path: `dist`,
      filename: `app.js`
    },

    module: {
      loaders: [
        {
          test: /\.ts(x?)$/,
          loader: `babel?presets=es2015!ts-loader`,
          include: [
            join(process.cwd(), `${project}`)
          ]
        }
      ],
      preLoaders: [
        {
          test: /\.js$/,
          loader: `source-map-loader`,
          include: [
            join(process.cwd(), `${project}`)
          ]
        }
      ]
    },

    plugins: prod ? [
      new webpack.optimize.UglifyJsPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.optimize.DedupePlugin()
    ] : [],

    resolve: {
      extensions: [``, `.webpack.js`, `.web.js`, `.jsx`, `.js`, `.tsx`, `.ts`]
    },

    ts: {
      configFileName:`${project}/src/tsconfig.json`
    }
  };
};