import { IDockerClient, IMapping, IShellClient } from './defs';
import { ShellClient } from './shell_client';

export class DockerClient implements IDockerClient {
	
	constructor(private shell: IShellClient) { }

	async pull(imageName: string) {
		await this.shell.$(`docker pull ${imageName}`);
	}

	async buildImage(imageName: string, workspace: string, dockerFile?: string, buildArgs?: IMapping[]) {
		let formattedBuildArgs = '';

		if (buildArgs) {
			buildArgs.forEach(arg => formattedBuildArgs += `--build-arg ${arg.toString('=')} `);
		}

		await this.shell.$(`docker build -t ${imageName} ${formattedBuildArgs} ${this.shell.optFlag('-f', dockerFile)} ${workspace}`);
	}

	async startContainer(containerName: string, imageName: string, ports: IMapping[], services?: IMapping[], volume?: IMapping, cmd?: string) {
		// docker pull mongo:latest
		let formattedServices = services ? services[0].toString() : null,
			formattedPorts = ports ? ports[0].toString() : null, //TODO: make this work for multiple mappings
			formattedVolume = volume ? volume.toString() : null;
		
		await this.shell.$(`docker run --name ${containerName} ${this.shell.optFlag('--link', formattedServices)} ${this.shell.optFlag('-v', formattedVolume)} ${this.shell.optFlag('-p', formattedPorts)} -d ${imageName} ${this.shell.opt(cmd)}`);
	}

	async attatchToContainer(containerName: string, cmd?: string) {
		// does this need to be a spawn instead of exec?
		await this.shell.$(`docker exec -it ${containerName} ${this.shell.opt(cmd)}`);
	}

	async removeAllStoppedContainers() {
		let containers = await this.getAllStoppedContainers();

		for(let id of containers) {
			await this.removeContainer(id);
		}
	}

	async removeContainer(container_id: string) {
		await this.shell.$(`docker rm ${container_id}`);
	}

	async stopContainer(container_id: string) {
		await this.shell.$(`docker stop ${container_id}`);
	}

	async stopAllContainers() {
		let containers = await this.getRunningContainers();

		for(let id of containers) {
			await this.stopContainer(id);
		}
	}

	async getAllUntaggedImages(): Promise<string[]> {
		// TODO: get this output hooked up and seperated into lines
		return (await this.shell.$ave(`docker images -q -f dangling=true`))
			.trim()
			.split(/\s+/)
			.filter(id => !!id);
	}

	async getRunningContainers(): Promise<string[]> {
		return (await this.shell.$ave(`docker ps -q`))
			.trim()
			.split(/\s+/)
			.filter(id => !!id);
	}

	async getAllStoppedContainers(): Promise<string[]> {
		let allContainers = await this.getAllContainers(),
			runningContainers = await this.getRunningContainers();

		return allContainers.filter(id => id 
			&& (!runningContainers.length || runningContainers.indexOf(id) !== -1));
	}

	async getAllContainers(): Promise<string[]> {
		return (await this.shell.$ave(`docker ps -a -q`))
			.trim()
			.split(/\s+/)
			.filter(id => !!id);
	}

	async getAllExitedContainers(): Promise<string[]> {
		return (await this.shell.$ave(`docker ps --filter status=exited -q`))
			.trim()
			.split(/\s+/)
			.filter(id => !!id);
	}

	async removeImage(image_id: string) {
		await this.shell.$(`docker rmi ${image_id}`);
	}

	async saveImage(image_id: string, out_file: string) {
		await this.shell.$(`docker save --output ./${out_file} ${image_id}`);
	}

	async removeAllUntaggedImages() {
		let containers = await this.getAllUntaggedImages();
		
		for(let id of containers) {
			await this.removeImage(id);
		}
	}

	async resetDockerEnv() {
		await this.stopAllContainers();
		await this.removeAllStoppedContainers();
		await this.removeAllUntaggedImages();
	}

	async copyFromContainer(container: string, from: string, to: string) {
		await this.shell.$(`docker cp ${container}:${from} ${to}`);
	}

	async createContainer(container: string, image: string) {
		await this.shell.$(`docker create --name ${container} ${image}`)
	}
}



