import { IMapping } from './defs';


export class Mapping implements IMapping {

	constructor(public from: string, public to: string) { }

	toString(delim? : string) {
		return `${this.from}${delim || ':'}${this.to}`;
	}
}