import { IShellClient } from './defs';
import { exec, spawn } from 'child_process';

export class ShellClient implements IShellClient {
	
	private remoteUser: string = null;
	private remoteAddress: string = null;
	private shouldLog: boolean = true;

	async $(cmd: string, args?: string[], saveOutput=false): Promise<string> {
		if (this.remoteUser && this.remoteAddress) {
			return await this.$aveh(cmd, args, saveOutput);
		}

		return new Promise<string>((resolve, reject) => {
			if (!args) {				
				args = cmd.split(/\s+/);
				cmd = args.shift();
			}

			if (this.shouldLog) {
				console.log(`\n# ${cmd} ${args.join(' ')}\n`);
			}

			let proc = spawn(cmd, args),
				stdout = '';

			this.setupProcess(proc, stdout, resolve, saveOutput);
		});
	}

	async $aveh(cmd: string, args?: string[], saveOutput=false): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			let proc = spawn('ssh', [`${this.remoteUser}@${this.remoteAddress}`]),
				stdout = '';

			this.setupProcess(proc, stdout, resolve, saveOutput);

			if (args) {
				cmd = args.join(' ');
			}

			proc.stdin.write(`${cmd} && exit || exit\n`);
		});
	}

	setupProcess(proc, stdout, resolve, saveOutput) {
		proc.stdout.on('data', (data) => {
			if (saveOutput) {
				stdout += data.toString();
			}

			console.log(data.toString());
		});

		proc.stderr.on('data', (data) => {
			console.log(data.toString());
		});

		proc.on('close', (code) => {
		  	if (code) {
		  		console.log(`process exited with: ${code}`);
		    	process.exit(1);
		    }

		    resolve(stdout);
		});
	}

	async $ave(cmd: string, args?: string[]): Promise<string> {
		return await this.$(cmd, args, true);
	}

	async cd(path: string) {
		await this.$(`cd ${path}`);
	}

	async mkdir(path: string) {
		await this.$(`mkdir -p ${path}`);
	}

	startRemoteSession(remoteUser: string, remoteAddress: string) {
		this.remoteUser = remoteUser;
		this.remoteAddress = remoteAddress; 
	}

	// IN HERE JUST CALL SSH...PUMP EACH COMMAND INTO THE STDIN
	// || exit for the first n-1 and && exit || exit for the last so it kills the ssh process popping us back into the build workflow
	async remote(remoteUser: string, remoteAddress: string, session: (shell: IShellClient) => void) {
		this.startRemoteSession(remoteUser, remoteAddress);

		await session(this);

		this.endRemoteSession();
	}

	beLoud() {
		this.shouldLog = true;
	}

	beQuiet() {
		this.shouldLog = false;
	}

	endRemoteSession() {
		this.remoteUser = this.remoteAddress = null;
	}

	optFlag(flag: string, value: string): string {
		return value ? `${flag} ${value}` : '';
	}

	opt(value: string): string {
		return value || '';
	}
}