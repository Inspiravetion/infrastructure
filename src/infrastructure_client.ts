import { IDockerClient, IShellClient } from './defs';
import { DockerClient } from './docker_client';
import { ShellClient } from './shell_client';
import { Mapping } from './mapping';
import { join } from 'path';

const CLUSTER_USER = '$CLUSTER_USER';
const CLUSTER_VM = '$CLUSTER_VM';
const CI_BUILD_REF = '$CI_BUILD_REF';
const DEPLOYMENT_IMAGE_SUFFIX = 'prod';

export class InfrastructureClient {

	private dockerClient: IDockerClient;
	private shell: IShellClient;
	private config;

	constructor() {
		this.shell = new ShellClient();
		this.dockerClient = new DockerClient(this.shell);

		this.config = require(join(process.cwd(), './infrastructure.js'));
	}

	public get port() : string {
		return this.config.port || '3000';
	}

	async startDB() {
		// await this.dockerClient.pull('mongo:latest');
		await this.dockerClient.startContainer(
			'mongo-service',
			'mongo',
			[
				new Mapping('27017', '27017')
			],
			null,
			new Mapping('/tmp/mongo', '/data'),
			'mongod --smallfiles'
		);
	}

	async exportDB() {
		await this.dockerClient.attatchToContainer('mongo', 'sh');
		await this.shell.$(`mongodump --db test --out /data/backup`);
		await this.shell.$(`exit`);
	}

	async importDB() {
		await this.dockerClient.attatchToContainer('mongo', 'sh');
		await this.shell.$(`mongorestore --db test-restored /data/backup/test`);
		await this.shell.$(`exit`);
	}

	async startWebApp(appName: string) {
		await this.dockerClient.startContainer(
			`${appName}-app`, 
			appName,
			[
				new Mapping(this.port, '3000')
			],
			[
				new Mapping('mongo-service', 'mongo-service')
			]
		);
	}

	async startEcosystem() {
		await this.startDB();
		//get this and rest of config from package.json
		// this.startWebApp('pensieve');
	}

	async stopDB() {
		await this.dockerClient.stopContainer('mongo');
	}

	async stopWebApp(appName: string) {
		await this.dockerClient.stopContainer(`${appName}-app`);
	}

	async stopEcosystem() {
		await this.dockerClient.stopAllContainers();
	}

	async buildDB() {
		// na..
	}

	async buildWebApp(appName: string) {
		await this.shell.$(`gulp build`);
	}

	async buildWebAppDeploymentImage(appName: string, buildContext?: string, dockerFile?: string) {
		await this.dockerClient.buildImage(appName, buildContext || `.`, dockerFile || `./prod.dockerfile`, [new Mapping('PORT', `${this.port}`)]);
	}

	async buildWebAppBuildImage(appName: string) {
		await this.dockerClient.buildImage(appName, `.`, `./build.dockerfile`);
	}

	async buildEcosystem() {
		await this.buildDB();
		// this.buildWebApp();
	}

	async jumpstartEcosystem() {
		await this.buildEcosystem();
		await this.startEcosystem();
	}

	async resetEnv() {
		await this.dockerClient.resetDockerEnv();
	}

	async scpBuildArtifactsToCluster() {
		let paths = [
			'dist/',
			'public/',
			'jspm_packages/',
			'node_modules/',
			'jspm_config.js',
			'package.json'
		].join('');

		await this.shell.$(`scp ${paths} ${CLUSTER_USER}@${CLUSTER_VM}:~/${CI_BUILD_REF}/.`)
	}

	async sshIntoClusterVM() {
		await this.shell.$(`ssh ${CLUSTER_USER}@${CLUSTER_VM}`);
		await this.shell.cd(`~/${CI_BUILD_REF}`);
	}

	async deployWebApp(appName: string) {
		//if dist/ doesn't exist build first
		await this.scpBuildArtifactsToCluster();
		await this.sshIntoClusterVM();
		await this.buildWebAppDeploymentImage(appName);
		await this.startDB();
		await this.startWebApp(appName);
	}
	
	// NEW STUFF
	async buildCI(appName: string) {
		this.shell.beQuiet();
		
		await this.dockerClient.buildImage(appName, '.', './build.dockerfile', [new Mapping('JSPM_GITHUB_AUTH_TOKEN', `${process.env.JSPM_GITHUB_AUTH_TOKEN}`)]);
		
		this.shell.beLoud();
		
		await this.dockerClient.createContainer(appName, appName);
		await this.shell.mkdir('build_output');
		await this.dockerClient.copyFromContainer(appName, './build', './build_output');
	}

	async cleanCI(appName: string) {
		await this.dockerClient.removeContainer(appName);
	}

	async deployCI(appName: string) {
		let deployImage = `${appName}_${DEPLOYMENT_IMAGE_SUFFIX}`,
			deployTar = `${appName}_${process.env.CI_BUILD_REF}.tar`,
			scpTarToCluster = `scp -i ~/.ssh/id_rsa ./${deployTar} ${process.env.CLUSTER_USER}@${process.env.CLUSTER_VM}:~/${deployTar}`,
			appContainer = `${appName}-app`;
 
		await this.buildWebAppDeploymentImage(deployImage, './build_output/build', './build_output/build/prod.dockerfile');
		await this.dockerClient.saveImage(deployImage, deployTar);
		await this.shell.$(scpTarToCluster);
		
		this.shell.startRemoteSession(process.env.CLUSTER_USER, process.env.CLUSTER_VM);
		
		await this.shell.$(`docker load --input ./${deployTar}`);
		// Check if conrainer with the same name exists and stop/rm it if it does
		await this.shell.$(`docker inspect --format='{{ .State.Running }}' '${appContainer}' && docker stop '${appContainer}' && docker rm '${appContainer}' || echo '# starting up'`)
		// Run the deployment image
		await this.shell.$(`docker run --name '${appContainer}' --link mongo-service:mongo-service -p ${this.port}:3000 -d '${deployImage}'`)
		// Remove the tar file
		await this.shell.$(`rm ./${deployTar}`);
		// Remove all stopped containers but leave their volumes
		// `docker rm $(docker ps --filter status=exited -q 2>/dev/null) || echo '# no loose docker containers to remove.'`
		// Remove old images that don't have any containers running
		// `docker rmi $(docker images --filter dangling=true -q 2>/dev/null) || echo '# no loose docker images to remove.'`
		this.shell.endRemoteSession();
	}

	async testSSH () {
		this.shell.startRemoteSession('inspiravetion', 'buildvm');
		await this.shell.$('touch sshtest.boob');
		this.shell.endRemoteSession();
	}
}