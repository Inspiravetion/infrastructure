
# RUNNER SETUP
- https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/linux-repository.md
- http://docs.gitlab.com/ce/ci/runners/README.html
- http://docs.gitlab.com/ce/ci/docker/using_docker_build.html

# GITHUB TOKEN(https://github.com/settings/tokens)

# JSPM TOKEN(https://github.com/jspm/jspm-cli/blob/master/docs/registries.md)

# DEPLOY KEYS
- https://gitlab.com/Inspiravetion/pensieve/deploy_keys
- http://docs.gitlab.com/ce/ssh/README.html#deploy-keys

# ENABLE_RUNNER(https://gitlab.com/Inspiravetion/pensieve/runners)

# Getting Docker(ssh setup)
- http://superuser.com/questions/235297/allow-specific-user-permission-to-read-write-my-folder
- http://stackoverflow.com/questions/9636198/ssh-id-rsa-failed-permission-denied
- http://docs.gitlab.com/ce/ci/docker/using_docker_build.html

# smarticles:
- dynamic, smart articles, with a beautiful ui that allows for different versions of an article and better links to things...think markdown but with animations and dynamic content changes...like flipping through versions of an article written for people who take one of two stand points
